import numpy as np
import cv2
import pymouse
import pynput
import pylab
import matplotlib

refPt = []
cropping = False

#for controlling the camera's
def Camera():
    cap = cv2.VideoCapture(1)
    for i in range(0,10):
        print cap.get(i)
    return cap

def GetCameraParams(IM):
    heigth = len(IM)
    width = len(IM[0])
    return heigth, width

#for the filter of the image or movie
def FilterIM(IM):
    #conversion to hsv value for increased filter precision
    hsv = cv2.cvtColor(IM, cv2.COLOR_BGR2HSV)
    #upper and lower bound masking values, later this will need IR filtering
    lower_blue = np.array([90,70,80])
    upper_blue = np.array([150,150,210])
    lower_red = np.array([0,0,100])
    upper_red = np.array([100,100,255])
    lower_IR = np.array([0,0,230])
    upper_IR = np.array([150,150,255])
    #another idea here is to apply background filtering
    
    #creating the mask/filtered image
    mask = cv2.inRange(hsv,lower_IR,upper_IR)
    #here the contour points are chosen
    ret,thresh = cv2.threshold(mask,127,255,0)
    #im2, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    
    return thresh#, contours

def BlobDetector(IM):
    j = 0
    x = []
    y = []
    IM=cv2.bitwise_not(IM)
    params = cv2.SimpleBlobDetector_Params()
    #these param settings are needed to make the detector robust enough
    #especially convexity is needed
    params.minThreshold = 1
    params.maxThreshold = 200
    params.minCircularity = 0
    params.minConvexity = 0
    detector = cv2.SimpleBlobDetector_create(params)
    keypoints = detector.detect(IM)
    #know how many blob points should be saved
    i = len(keypoints)
    if keypoints != []:
        for j in range(0,i):
            x.append(keypoints[j].pt[0]) #i is the index of the blob you want to get the position
            y.append(keypoints[j].pt[1])
    else:
        x = 0
        y = 0
    
    #makes the images with red circles around them, maybe not needed later or refined
    blobim = cv2.drawKeypoints(IM, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    return blobim, i, x, y

def MouseInput(i):
    #ginput causes several warnings, why?
    point = pylab.ginput(i)    
    return point

def PointCoupling(ClkMtx,BlbMtx):
    #pos has to be filled for a pre-defined size and then replaced with value
    pos = []
    posx = []
    posy = []
    for mincombs in range(0,min(len(ClkMtx),len(BlbMtx))):
        pos.insert(mincombs,0)
    distance = np.empty([len(ClkMtx),len(BlbMtx)])
    #see matlab minimal choice method for the coupling of mouse click to contour
    for imarkers in range (0,len(ClkMtx)):
        for iblob in range (0,len(BlbMtx)):
            disx = ClkMtx[imarkers][0] - BlbMtx[iblob][0]
            disy = ClkMtx[imarkers][1] - BlbMtx[iblob][1]
            disxy = (disx**2 + disy**2) ** (0.5)
            distance[imarkers,iblob] = disxy
    for mincombs in range(0,min(len(ClkMtx),len(BlbMtx))):
        ind = np.unravel_index(np.argmin(distance, axis=None), distance.shape)
        #an extra step in case distances are exactly equal might be needed later
        #next a statement to see if the prediction is precise enough might be implimented
        
        #a matrix of all positions should be stored here with the values from BlbMtx
        #pos.insert(ind[0],BlbMtx[ind[1]])
        pos[ind[0]] = BlbMtx[ind[1]]
        
        #ugly way but None or inf dont seem to work
        distance[ind] = 10000       
    #next the minimum combination has to be coupled and that entry deleted
    for i in range(0,len(pos)):
        posx.append(pos[i][0])
        posy.append(pos[i][1])
        
    return pos, posx, posy

def BuildFullPos(pos, fullpos):
    if fullpos == []:
        fullpos = np.array(pos)
        fullpos = np.atleast_3d(fullpos)
    else:
        pos = np.atleast_3d(np.array(pos))
        fullpos = np.dstack((fullpos,pos))
    return fullpos

def PointPrediction():
    #use per frame with longer history to implement acceleration and speed if needed
    return

#loaded in should be pos for camera1 and pos for camera2
def AngleCalculation(posx,posy,height, width):
    #still all theoretical
    anglex = []
    angley = []
    CamFOVX = 75/2
    CamFOVY = 59.84/2
    imdisx = (width/2)/np.tan(np.deg2rad(CamFOVX))
    imdisy = (height/2)/np.tan(np.deg2rad(CamFOVY))
    for i in range(0,len(posx)):
        anglex.append(np.tan(np.deg2rad((posx[i]-(width/2))/imdisx)))
        angley.append(np.tan(np.deg2rad((posy[i]-(height/2))/imdisy)))

    return anglex, angley
    
def RealposCalculation(alphax, alphay, betax, betay):
    R = np.matrix([[ 0.74995611, 0.11521298, 0.65137685], [-0.05891293, 0.99243538, -0.10770928], [-0.65885894, 0.04240272, 0.75107051]])
    T = [-382.96882325, 9.58848379, 169.40167489]
    invR = np.linalg.inv(R)
    for i in range(0,len(betax)):
        BetaVector = [betax[i],betay[i],1]
        BetaVector = np.array(BetaVector)
        Beta_corrected = np.inner(invR,BetaVector.transpose())
        Beta_corrected = np.array(Beta_corrected)
        RealBetaX = Beta_corrected[0][0]
        RealBetaY = Beta_corrected[0][1]
        x = (T[0]+T[2]*np.tan(np.deg2rad(RealBetaX)))/ (1-(1/np.tan(np.deg2rad(alphax[i]))))
        x = x.transpose()
        y = x*np.tan(np.deg2rad(alphax[i]))
        z = (T[1]+T[2]*np.tan(np.deg2rad(RealBetaY)))/ (1-(1/np.tan(np.deg2rad(alphay[i]))))
        z = z.transpose()
        Realpos = np.dstack((x,y,z))
        print Realpos

    #this was the old method where everything was done in arrays
    #though this becomes very messy and some calculations become incorrect with matrix and vector methods
    #so a for loop is used instead
    
    #onesarray = [1] * len(betax)
    #betaVector = [betax,betay,onesarray]
    #betaVector = np.array(betaVector)
    #beta_corrected = np.inner(invR,betaVector.transpose())
    #betax = beta_corrected[0]
    #betay = beta_corrected[1]
    #x = (T[0]+T[2]*np.tan(np.deg2rad(betax)))/ (1-(1/np.tan(np.deg2rad(alphax))))
    #x = x.transpose()
    #print alphax
    #y = x*np.tan(np.deg2rad(alphax))
    #z = (T[1]+T[2]*np.tan(np.deg2rad(betay)))/ (1-(1/np.tan(np.deg2rad(alphay))))
    #z = z.transpose()
    #print x
    #print y
    #print z
    
    return Realpos
#some points for attention:
#loading in video is different then in matlab, frame info is needed most of all
#to display etc now a while loop is used, preferably this will go another way later

#https://stackoverflow.com/questions/25359288/how-to-know-total-number-of-frame-in-a-file-with-cv2-in-python
#for the information of videos loaded in

#https://stackoverflow.com/questions/39083360/why-cant-i-do-blob-detection-on-this-binary-image
#for blob detection

#https://stackoverflow.com/questions/11420748/setting-camera-parameters-in-opencv-python
#for indices to get properties of cap.get
