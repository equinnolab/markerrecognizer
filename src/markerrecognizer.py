#this project is used to check how to load in videos, images and webcam feeds
#with correct parameters and how to optimally filter and do calculations on them

#imports
import numpy as np
import cv2
import time
import pynput
import matplotlib
import engine as engine
import Tkinter
import scipy.io

#loading of video
cap1 = cv2.VideoCapture('D:\Downloads\zomerbaantje\start project\ImageResearch\\cam1test1.avi')
cap2 = cv2.VideoCapture('D:\Downloads\zomerbaantje\start project\ImageResearch\\cam2test1.avi')
#fgbg = cv2.createBackgroundSubtractorMOG2()
#a parameter so the input only has to be given once
InputDone = False
fullpos1 = []
fullpos2 = []
frame = 50

while(True):
    cap1.set(1,frame)
    cap2.set(1,frame)
    frame = frame + 1
    #reads the data from the webcam/video
    ret1,IM1 = cap1.read()
    ret2,IM2 = cap2.read()
    height,width = engine.GetCameraParams(IM1)
    #this creates a window with pre-determined size to be able to work with it
    cv2.resizeWindow('IM', 360,640)
    cv2.resizeWindow('mask', 360,640)
    #resizing the image for display purposes
    IMd1 = cv2.resize(IM1,(360,640))
    IMd2 = cv2.resize(IM2,(360,640))    
    mask1 = engine.FilterIM(IMd1)
    mask2 = engine.FilterIM(IMd2)
    #mask1 = fgbg.apply(mask1)
    #mask2 = fgbg.apply(mask2)
    if frame > 50:
        blobim1, i1, xblob1, yblob1 = engine.BlobDetector(mask1)
        blobim2, i2, xblob2, yblob2 = engine.BlobDetector(mask2)
        BlbMtx1 = np.column_stack((xblob1,yblob1))
        BlbMtx2 = np.column_stack((xblob2,yblob2))
        if ret1 == True:
            cv2.imshow('IM1',IMd1)
            cv2.imshow('blobim1',blobim1)   
            cv2.imshow('IM2',IMd2)
            cv2.imshow('blobim2',blobim2)   
        if InputDone == False:
            #set the plot to a certain frame
            clickx1 = []
            clicky1 = []
            matplotlib.pyplot.imshow(mask1, cmap='Greys',  interpolation='nearest')
            point1 = engine.MouseInput(i1)
            clickx2 = []
            clicky2 = []
            matplotlib.pyplot.imshow(mask2, cmap='Greys',  interpolation='nearest')
            point2 = engine.MouseInput(i2)
            InputDone = True
            for j in range(0,i1):
                clickx1.append(point1[j][0])
                clicky1.append(point1[j][1])
            for j in range(0,i2):
                clickx2.append(point2[j][0])
                clicky2.append(point2[j][1])
            #destroy figure
            matplotlib.pyplot.close()
            ClkMtx1 = np.column_stack((clickx1,clicky1))
            ClkMtx2 = np.column_stack((clickx2,clicky2))
            #makes a matrix in the following structure:
            #[x1, y1]
            #[x2, y2]... etc
        #mindistance method (point coupling)
        pos1,posx1,posy1 = engine.PointCoupling(ClkMtx1,BlbMtx1)
        fullpos1 = engine.BuildFullPos(pos1, fullpos1)
        ClkMtx1 = pos1
        pos2,posx2,posy2 = engine.PointCoupling(ClkMtx2,BlbMtx2)
        fullpos2 = engine.BuildFullPos(pos2, fullpos2)
        ClkMtx2 = pos2
        alphax,alphay = engine.AngleCalculation(posx1,posy1,height, width)
        betax,betay = engine.AngleCalculation(posx2,posy2,height, width)
        RealPos = engine.RealposCalculation(alphax, alphay, betax, betay)
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        scipy.io.savemat('D:\\Downloads\\zomerbaantje\\start project\\ImageResearch\\fullpos.mat', mdict = {'RealPos' : RealPos})
        break

#steps:
#ask what frame to use? a way to check frames
#save the click coordinates in an array
#save the blobs in an array
#minimal distance choices
#show the combinations (blob connects to clicked marker since you know what click is what marker